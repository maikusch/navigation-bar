const gulp = require("gulp");
const sass = require("gulp-sass");
const del = require("del");
const GulpClient = require("gulp");
const publicPatch = "public/";

gulp.task('styles', () => {
    return gulp.src(publicPatch + "sass/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest(publicPatch + "./css"));
});

gulp.task("clean", () => {
    return del([
        publicPatch + "css/main.css"
    ]);
});

gulp.task("watch", () => {
    gulp.watch(publicPatch + "sass/**/*.scss", (done) => {
        gulp.series(["clean", "styles"])(done);
    });
});

gulp.task('default', gulp.series(["clean", "styles"]));